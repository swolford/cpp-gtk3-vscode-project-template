# cpp-gtk3-vscode-project-template

A Visual Studio Code project template for GTK 3 C++ application development. It requires `make`, `gcc`, and `pkg-config` to build.

# Usage

Clone the repository into your working directory.

```
git clone https://gitlab.com/swolford/cpp-gtk3-vscode-project-template.git my-new-project
```

Open the folder in Visual Studio Code. Build and run to make sure everything works. The output files will be in the `bin` directory.


# Intellisense

This project has the GTK library headers added to Intellisense. See `.vscode/c_cpp_properties.json` for a list of packages needed.

This list is formatted from the output of `pkg-config --cflags gtk+-3.0` and yours may be different. If you have issues with Intellisense not working, run the command and replace the list of include paths with the ones installed on your system.

# License
This project is in the public domain. See `LICENSE.txt` for details.
